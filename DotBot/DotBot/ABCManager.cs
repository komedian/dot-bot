﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace DotBot
{
    class ABCManager
    {
        List<ABCDictionary> dictionarys;

        public ABCManager()
        {
            dictionarys = new List<ABCDictionary>();
        }

        public void AddABCDictionary(ABCDictionary newDictionary)
        {
            dictionarys.Add(newDictionary);
        }

        public string GetJsonStringFromABC(ABCDictionary dict, string message)
        {
            string outputString = "{";
            string letters = new string(message.Where(c => Char.IsLetter(c)).ToArray());

            SymbolCounter[] usableSimbols = CalculateStatistics(dict, letters);

            for (int i = 0; i < usableSimbols.Length; i++)
            {
                if (i == usableSimbols.Length - 1)
                    outputString += usableSimbols[i].ToString();
                else
                    outputString += usableSimbols[i].ToString() + ",\n";
            }
            

            return outputString + "}";
            
        }

        public string GetStatisticFromString(string messages)
        {
            string statistic = "";

            CalculateCounters(messages);

            for (int i = 0; i < dictionarys.Count; i++)
                statistic += GetJsonStringFromABC(dictionarys[i], messages);

            RefreshDataDictionarys();

            return statistic;
        }

        private void CalculateCounters(string messages)
        {
            foreach (var symbol in messages)
            {
                for (int i = 0; i < dictionarys.Count; i++)
                    dictionarys[i][symbol]++;
            }
        }

        private void RefreshDataDictionarys()
        {
            for (int i = 0; i < dictionarys.Count; i++)
                dictionarys[i].RefreshCounters();
        }

        private SymbolCounter[] CalculateStatistics(ABCDictionary dict, string message)
        {
            SymbolCounter[] usableSymbols = dict.GetUsableSymbols();

            for (int i = 0; i < usableSymbols.Length; i++)
            {
                usableSymbols[i].Frequency = usableSymbols[i].Frequency / message.Length;
            }

            return usableSymbols;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DotBot
{
    [DataContract]
    public class SymbolCounter
    {
        public SymbolCounter()
        {
        }
        public SymbolCounter(char symbol, double count)
        {
            Symbol = symbol;
            Frequency = count;
        }
        [DataMember]
        public char Symbol;
        [DataMember]
        public double Frequency;

        public override string ToString()
        {
            return $"{Symbol}:'{Frequency:0.00000}'";
        }

    }
}

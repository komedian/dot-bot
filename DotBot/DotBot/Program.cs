﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotBot
{
    class Program
    {
        static void Main(string[] args)
        {
            ABCDictionary dic = ABCDictionary.CreateABCDictionary("Ru", "../../ABC/Ru.json");
            ABCManager manager = new ABCManager();
            manager.AddABCDictionary(dic);
            TwitterBot bot = TwitterBot.InitializeTwitterBot();



            while (true)
            {
                string name = Console.ReadLine();

                if (name == string.Empty) break;

                int tweetsCount = 5;
                string timeLine = bot.ReadTimeLineSomePeople(name, tweetsCount);
                
                string statistic = manager.GetStatisticFromString(timeLine.ToLower());
                string outMessage = $"{name}, статистика для последних {tweetsCount} твитов: \n{statistic}";
                //bot.SendTweet(outMessage);
                //Console.WriteLine(timeLine);
                Console.WriteLine(outMessage);
            }
        }
    }
}

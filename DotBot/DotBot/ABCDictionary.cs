﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace DotBot
{
    public class ABCDictionary
    {
        SymbolCounter[] alphabet;

        ABCDictionary(string language, SymbolCounter[] chars)
        {
            Language = language;
            alphabet = chars;
        }

        public string Language { get; }

        public char this[int number]
        {
            get { return alphabet[number].Symbol; }
        }

        public double this[char symbol]
        {
            get
            {
                for (int i = 0; i < alphabet.Length; i++)
                {
                    if (symbol == alphabet[i].Symbol)
                        return alphabet[i].Frequency;
                }
                return -1;
            }
            set
            {
                for (int i = 0; i < alphabet.Length; i++)
                {
                    if (symbol == alphabet[i].Symbol)
                        alphabet[i].Frequency = value;
                }
            }
        }

        public void RefreshCounters()
        {
            for (int i = 0; i < alphabet.Length; i++)
                alphabet[i].Frequency = 0;
        }

        public SymbolCounter[] GetUsableSymbols()
        {
            List<SymbolCounter> usableSymbols = new List<SymbolCounter>();

            for (int i = 0; i < alphabet.Length; i++)
                if (alphabet[i].Frequency != 0) usableSymbols.Add(alphabet[i]);

            return usableSymbols.ToArray();
        }

        public static ABCDictionary CreateABCDictionary(string name, string path)
        {
            SymbolCounter[] chars;
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(SymbolCounter[]));
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                chars = (SymbolCounter[])jsonFormatter.ReadObject(fs);
            }

            return new ABCDictionary(name, chars);
        }
    }
}
